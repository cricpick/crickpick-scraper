var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app = express();

app.get('/scrape-match-results', function (req, res) {

    // Let's scrape Anchorman 2
    if (!req.query.matchId) {
        res.send('Please send matchId in query params');
        return;
    }


    url = 'http://www.espncricinfo.com/ci/engine/match/' + req.query.matchId + '.html';
    // url = 'http://localhost:8080/scorecard.html';
    request({
        url: url,
        headers: {
            'User-Agent': 'request'
        }
    }, function (error, response, html) {
        console.log(html);
        if (!error) {
            var $ = cheerio.load(html);
            var scoreCard = {};


            function populateScoreCard() {
                var i = 0;
                scoreCard.battingCard = {};
                $('.batting-table.innings').each(function () {
                    var a = [];
                    var teamName = $($(this).find('th')[1]).text().split('(')[0].trim();
                    $(this).find('tr').each(function () {
                        var b = $(this).find('td');
                        var c = {
                            batsman: $(b[1]).text().trim(),
                            dismissal: $(b[2]).text().trim(),
                            runs: $(b[3]).text(),
                            balls: $(b[4]).text(),
                            fours: $(b[5]).text(),
                            sixes: $(b[6]).text(),
                            strikeRate: $(b[7]).text()
                        };
                        c.batsman && a.push(c);
                    });
                    scoreCard.battingCard[teamName] = a;
                    i++;
                });

            }

            populateScoreCard();
        }

        // fs.writeFile('output.json', JSON.stringify(json, null, 4), function(err){
        //     console.log('File successfully written! - Check your project directory for the output.json file');
        // })

        res.send(scoreCard);
    });
});

app.listen('8081')
console.log('Magic happens on port 8081');
exports = module.exports = app;