var cheerio = require("cheerio");
var request = require('request');
var scoreCard = {};
request('http://localhost:8080/scorecard.html', function (error, response, body) {
    //console.log(body);
    if (!error && response.statusCode == 200) {
        var $ = cheerio.load(body);

        function populateScoreCard() {
            var i = 0;
            scoreCard.battingCard = [];
            $('.batting-table.innings tr').each(function () {
                var b = $(this).find('td');
                var c = {
                    batsman: $(b[1]).text(),
                    dismissal: $(b[2]).text(),
                    runs: $(b[3]).text(),
                    balls: $(b[4]).text(),
                    fours: $(b[5]).text(),
                    sixes: $(b[6]).text(),
                    strikeRate: $(b[7]).text()
                };
                scoreCard.battingCard[i] = c;
                i++;
            });

        }

        populateScoreCard();
        console.log(JSON.stringify(scoreCard));

    }

})